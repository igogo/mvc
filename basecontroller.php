<?php

class BaseController
{
    public function reload()
    {
        header("Refresh:0");
    }

    public function render($view, array $data)
    {
        $class = new \ReflectionClass(get_class($this));
        $className = strtolower($class->getShortName());
        $path = ROOT . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $className . DIRECTORY_SEPARATOR . $view . '.html';

        if (!file_exists($path)) {
            Application::error(404, 'Отображение {' . $path . '} не найдено');
        }

        $html = file_get_contents($path);
        $html = str_replace(array_map(function($key) {
            return '{{' . $key . '}}';
        }, array_keys($data)), $data, $html);

        echo $html;
    }
}

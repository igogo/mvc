<?php

class Application
{
    protected static $_instance;

    public $defaultRoute = 'main/index';
    public $errorAction = 'main/error';

    public static function getInstance(array $config = [])
    {
        if (!self::$_instance) {
            self::$_instance = new Application($config);
        }

        return self::$_instance;
    }

    private function __construct(array $config = [])
    {
        set_exception_handler([$this, 'runErrorAction']);
        set_error_handler([$this, 'runErrorAction']);

        foreach ($config as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    public function run()
    {
        $route = Router::getRoute($this->defaultRoute);
        list($controller, $action, $args) = Router::getPrepearedAction($route);

        $this->runAction($controller, $action, $args);
    }

    public function runAction($controller, $action, array $args = [])
    {
        if (controller_exists($controller)) {
            $controller = new $controller();

            if (!($controller instanceof BaseController)) {
                Application::error(500, 'Контроллер должен быть наследован от BaseController');
            }

            if (is_object($controller) && method_exists($controller, $action)) {
                call_user_func_array([$controller, $action], $args);
                return true;
            }
        }

        Application::error(404, 'Страница не найдена');
    }

    public static function error($code, $msg)
    {
        throw new Exception($code . ' ' . $msg);
    }


    public function runErrorAction($err, $message = null)
    {
        list($controller, $action, $args) = Router::getPrepearedAction($this->errorAction);
        if ($err instanceof Exception) {
            $message = $err->getMessage();
        }

        $this->runAction($controller, $action, array_merge($args, [$message]));
    }
}

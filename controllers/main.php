<?php

namespace controllers;

use \models\Model;
use Application;
use BaseController;

class Main extends BaseController
{
    public function actionIndex()
    {
        $data = Model::getData();
        $this->render('view', $data);
    }

    public function actionForm()
    {
        $data = Model::getData();

        if (isset($_POST['data'])) {
            Model::setData($_POST['data']);
            $this->reload();
        }

        $this->render('form', $data);
    }

    public function actionError($error)
    {
        $this->render('error', ['message' => $error]);
    }
}

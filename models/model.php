<?php

namespace models;

class Model
{
    public static function getData()
    {
        return json_decode(file_get_contents(dirname(__FILE__) . '/data.json'), true);
    }

    public static function setData(array $data)
    {
        return file_put_contents(dirname(__FILE__) . '/data.json', json_encode($data));
    }
}

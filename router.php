<?php

class Router
{
    public static function getRoute($defaultRoute)
    {
        $route = isset($_GET['r']) && !empty($_GET['r']) ? $_GET['r'] : $defaultRoute;
        return $route;
    }

    public static function getPrepearedAction($route)
    {
        $route = explode('/', $route);
        $controller = 'controllers\\' . ucfirst($route[0]);
        $action = isset($route[1]) ? 'action' . ucfirst($route[1]) : 'actionIndex';

        $args = $_GET;
        if (isset($args['r'])) {
            unset($args['r']);
        }

        return [$controller, $action, $args];
    }
}

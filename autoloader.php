<?php

function toPath($class_name)
{
    return ROOT . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, strtolower($class_name)) . '.php';
}

function controller_exists($class_name)
{
    return file_exists(toPath($class_name));
}

function __autoload($class_name)
{
    require_once toPath($class_name);
}
